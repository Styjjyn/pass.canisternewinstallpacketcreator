﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator
{
    public class ActionState_VerifyMachineNumber : ActionState
    {
        OleDbConnection connection;

        public ActionState_VerifyMachineNumber(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Verify Machine Number";

        public override string StateDescription => throw new NotImplementedException();

        

        public override bool Enter()
        {
            Form1.UpdateActivityLog("Establishing connection to database...");
            connection = new OleDbConnection();

            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                            @"Data source= \\fserver\Shared\Manufacturing\Production\!Public\Database\Manufacturing Production Database.accdb";
            Form1.UpdateActivityLog("...connection Established to database...");

            return true;
        }

        public override bool Exit()
        {

            return true;
        }

        public override bool Run()
        {
            Form1.SetCurrentToMarqee();
            Form1.UpdateActivityLog("Running Access query...");
            using (OleDbCommand cmd = new OleDbCommand())
            {
                Form1.UpdateActivityLog("Opening connection to database...");
                connection.Open();
                Form1.UpdateActivityLog("...finished opening connection to database!");
                cmd.Connection = connection;

                cmd.CommandText = "SELECT TableCanisterData.Machine, " +
                                        "TableCanisterData.[No], " +
                                        "TableCanisterData.Mnemonic, " +
                                        "TableCanisterData.NDC, " +
                                        "TableCanisterData.Generic_Name, " +
                                        "TableCanisterData.Commercial_Name, " +
                                        "TableCanisterData.Label_Name, " +
                                        "TableCanisterData.Strength, " +
                                        "TableCanisterData.[Type of Tablet], " +
                                        "TableCanisterData.Drug_Color, " +
                                        "TableCanisterData.Manufacturer, " +
                                        "TableCanisterData.Form, " +
                                        "TableCanisterData.[Customer Name], " +
                                        "TableCanisterData.Asset, " +
                                        "TableCanisterData.[Asset type] " +
                                "FROM TableCanisterData " +
                                        "WHERE(((TableCanisterData.Machine) = " + Variables.MachineNumber + ")) "+
                                "ORDER BY TableCanisterData.[No] DESC";

                List<string> originalNDCs = new List<string>();
                Form1.UpdateActivityLog("Executing command...");
                using (OleDbDataReader dr = cmd.ExecuteReader())
                {
                    Form1.UpdateActivityLog("...Access Query executed.");
                    Form1.UpdateActivityLog("Storing Query info...");
                    while (dr.Read())
                    {
                        List<string> line = new List<string>();
                        line.Add("" + dr[Constants.headerName[0]]);
                        line.Add("" + dr[Constants.headerName[1]]);
                        line.Add("" + dr[Constants.headerName[2]]);
                        line.Add("" + dr[Constants.headerName[3]]);
                        line.Add("" + dr[Constants.headerName[4]]);
                        line.Add("" + dr[Constants.headerName[5]]);
                        line.Add("" + dr[Constants.headerName[6]]);
                        line.Add("" + dr[Constants.headerName[7]]);
                        line.Add("" + dr[Constants.headerName[8]]);
                        line.Add("" + dr[Constants.headerName[9]]);
                        line.Add("" + dr[Constants.headerName[10]]);
                        line.Add("" + dr[Constants.headerName[11]]);

                        if (Variables.AssetFolderName == null)
                        {
                            Variables.CustomerName = "" + dr["Customer Name"];
                            Variables.AssetNumber = "" + dr["Asset"];
                            Variables.AssetType = "" + dr["Asset type"];
                            Variables.AssetFolderName = Variables.MachineNumber + " - " + Variables.CustomerName + " Asset " + Variables.AssetNumber;

                        }

                        //Check for duplicate NDCs and 10 digit NDCs
                        if (originalNDCs.Contains(line[3]) == false && line[3].Length == 9)
                        {
                            //Add original NDCs and 9 digit NDCs to CDD eXport
                            originalNDCs.Add(line[3]);
                            Variables.CddExportData.Add(line);
                        }
                        else
                        {
                            //Add duplicate NDCs and 10 digit NDCs to the '...notadded.txt'
                            Variables.NdcsNotAdded.Add(line);
                        }

                    }
                    Form1.UpdateActivityLog("... finished storing Query info!");

                }
                Form1.UpdateActivityLog("closing database connection...");
                connection.Close();
                Form1.UpdateActivityLog("...database connection closed!");
                return true;
            }
        }
    }
}
