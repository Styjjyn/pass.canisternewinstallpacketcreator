﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_ValidatePrereqs : ActionState
    {
        public ActionState_ValidatePrereqs(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Verifying Prequisite Conditions";

        public override string StateDescription => "Checks for the existence of needed files and folders.";

        public override bool Enter()
        {
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            CheckFolder(Constants.PATH_FOLDER_PACMED, false);
            CheckFolder(Constants.PATH_APP_FOLDER, true);
            CheckFolder(Constants.PATH_APP_EXE_INPUT_FOLDER, true);
            CheckFolder(Constants.PATH_APP_LOG_FOLDER, true);
            CheckFolder(Constants.PATH_APP_CONFIG_FOLDER, true);

            if (CheckFolder(Constants.PATH_INNO_SETUP, false) == false)
            {
                ErrorHandler.Add(new Exception($"Inno Setup is not installed at the specified location: {Constants.PATH_INNO_SETUP}. Either install ISS at the location specified or change the .Config file entry to match the location."));
                return false;
            }

            if (CheckFolder(Constants.PATH_APP_ISS_FOLDER, false) == false)
            {
                ErrorHandler.Add(new Exception($"The folder at: {Constants.PATH_APP_LOG_FOLDER} was not found. Try rerunning the installer."));
                return false;
            }

            return true;
        }

        private bool CheckFolder(string path, bool createOnNotFound)
        {
            Form1.UpdateActivityLog("Verifying Folder: " + path);
            if (Directory.Exists(path) == false)
            {
                if (!createOnNotFound)
                    return false;

                Form1.UpdateActivityLog("Creating Folder: " + path);
                Directory.CreateDirectory(path);
                return false;
            }
            else
            {
                Form1.UpdateActivityLog("Folder found!");
                return true;
            }
        }
    }
}
