﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_Completed : ActionState
    {
        public ActionState_Completed(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Finished!!!";

        public override string StateDescription => "All tasks completed successfully!";

        public override bool Enter()
        {
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            Form1.ShowMessagePrompt("Success!",StateDescription);
            return true;
        }
    }
}
