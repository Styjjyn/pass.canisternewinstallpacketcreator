﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_PrepareExeCreation : ActionState
    {
        public ActionState_PrepareExeCreation(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Preparing EXE creation";

        public override string StateDescription => "Copies 'SelectedDrugList' and 'NDCs not included' to Inno Setup Script INPUT folder" +
                                                    "Removes 'NDCs not included' preexisiting in INPUT folder";

        

        public override bool Enter()
        {
            Form1.UpdateActivityLog("Removing preexisting files...");
            //remove preexisitng not included file
            DirectoryInfo dir = new DirectoryInfo(Constants.PATH_APP_EXE_INPUT_FOLDER);
            FileInfo f = dir.GetFiles(Constants.FILE_NAME_NDCS_NOT_INCLUDED).FirstOrDefault();

            if (f != null)
                f.Delete();

            //remove preexisting selected drug list
            f = dir.GetFiles(Constants.FILE_NAME_SELECTED_DRUG_LIST).FirstOrDefault();
            if (f != null)
                f.Delete();
            Form1.UpdateActivityLog("...finished removing preexisting files!");
            return true;
        }


        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            Form1.UpdateActivityLog("copying files from Asset folder to EXE input folder...");
            //Copy files from asset folder to exe input folder
            DirectoryInfo fromDir = new DirectoryInfo(Constants.PATH_FOLDER_PACMED + Variables.AssetFolderName + "\\Passware\\");
            DirectoryInfo toDir = new DirectoryInfo(Constants.PATH_APP_EXE_INPUT_FOLDER);

            Form1.UpdateActivityLog("...copying 'SelectedDrugList.txt' from Asset folder to EXE input folder...");
            FileInfo toCopy = fromDir.GetFiles(Constants.FILE_NAME_SELECTED_DRUG_LIST).FirstOrDefault();

            toCopy.CopyTo(toDir + "\\" + toCopy.Name);

            Form1.UpdateActivityLog("...copying 'SelectedDrugList.txt' from Asset folder to EXE input folder...");
            toCopy = fromDir.GetFiles(Constants.FILE_NAME_NDCS_NOT_INCLUDED).FirstOrDefault();
            if (toCopy != null)
                toCopy.CopyTo(toDir + "\\" + toCopy.Name);
            else
                Form1.UpdateActivityLog("...NOT copying 'NDCsNotIncluded.txt' from Asset folder to EXE input folder(doesn't exist ;))...");

            Form1.UpdateActivityLog("... finished copying files from Asset folder to EXE input folder");

            return true;
        }
    }
}
