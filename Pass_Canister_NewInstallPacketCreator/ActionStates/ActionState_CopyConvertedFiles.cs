﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_CopyConvertedFiles : ActionState
    {
        public ActionState_CopyConvertedFiles(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Copying converted files from email to shared drive";

        public override string StateDescription => throw new NotImplementedException();

        

        public override bool Enter()
        {
            foreach (Outlook.Attachment item in StateMachine.CddMail.Attachments)
            {
                item.SaveAsFile(Variables.MachinePasswareFolderPath + "\\" + item.FileName);

            }
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            return true;
        }
    }
}
