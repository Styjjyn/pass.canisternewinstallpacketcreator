﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_PerformExcelTasks : ActionState
    {
        public ActionState_PerformExcelTasks(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Formatting Cdd Export in Excel";

        public override string StateDescription => "Adding header names to Cdd Export. \nChanging Sheet name.";

        

        public override bool Enter()
        {
            Variables.CustomerXLSFilePath = Constants.PATH_FOLDER_PACMED + Variables.AssetFolderName + "\\" + Variables.AssetFolderName + ".xls";
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            Form1.SetCurrentToMarqee();
            Form1.UpdateActivityLog("Formatting Exported XLS...");
            CreateNewWorkBook(Variables.CustomerXLSFilePath);
            RenameWorksheet(1, "Label Sheet");
            AddHeadersToSheet(1, Constants.headerName);
            AddDataToSheet(1, Variables.CddExportData);
            SortData(1);
            Form1.UpdateActivityLog("... finished formatting Exported XLS!");
            return true;
        }

        static Excel.Application app;
        static Excel.Workbook workBook;
        static Excel.Worksheet workSheet;

        internal static void CreateNewWorkBook(string workbookPath)
        {
            app = new Excel.Application();
            workBook = app.Workbooks.Add();
            workBook.SaveAs(workbookPath, Excel.XlSaveConflictResolution.xlUserResolution);
        }

        internal static void RenameWorksheet(int index, string to)
        {
            workSheet = workBook.Worksheets[index];
            workSheet.Name = to;
            workBook.Save();
        }

        internal static void AddHeadersToSheet(int sheetIndex, string[] headersToAdd)
        {
            workSheet = workBook.Worksheets[sheetIndex];

            Excel.Range rangeToWrite = workSheet.Range["A1", "L1"];
            rangeToWrite.Value2 = headersToAdd;

            rangeToWrite.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
            workBook.Save();
        }

        internal static void AddDataToSheet(int sheetIndex, List<List<string>> dataToAdd)
        {
            workSheet = workBook.Worksheets[sheetIndex];

            Excel.Range rangeToWrite;
            List<string> data;
            for (int i = 0; i < dataToAdd.Count; i++)
            {
                data = dataToAdd[i];
                rangeToWrite = workSheet.Range["A" + (2 + i), "L" + (2 + i)];
                rangeToWrite.Value2 = data.ToArray();
            }
            rangeToWrite = workSheet.Range["A1", "L1"];
            rangeToWrite.Columns.AutoFit();
            rangeToWrite.Cells.Justify();
            workBook.Save();
        }

        internal static void SortData(int sheetIndex)
        {
            workSheet = workBook.Worksheets[sheetIndex];

            workSheet.Application.ActiveWindow.SplitRow = 1;
            workSheet.Application.ActiveWindow.FreezePanes = true;

            Excel.Range rangeToFilter = workSheet.Rows[1];

            Excel.Range sortRange = workSheet.Range["A2", "L" + (2 + Variables.CddExportData.Count)];
            sortRange.Sort(sortRange.Columns[2], Excel.XlSortOrder.xlAscending);
            workBook.Save();
            workBook.Close();
            app.Quit();
        }
    }
}
