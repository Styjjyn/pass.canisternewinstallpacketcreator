﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_OpenOutlookSilently : ActionState
    {
        

        public ActionState_OpenOutlookSilently(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Open outlook silently";

        public override string StateDescription => "Open outlook without presenting the GUI to the user. Immediately start polling for PDDB email";

        

        public override bool Enter()
        {
            Form1.UpdateActivityLog("Outlook initialization started...");
            StateMachine.outlook = null;

            if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
                StateMachine.outlook = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
            else
            {
                Form1.UpdateActivityLog("...Running new Outlook instance...");
                StateMachine.outlook = new Outlook.Application();
                Outlook.NameSpace nameSpace = StateMachine.outlook.GetNamespace("MAPI");
                nameSpace.Logon("","",Missing.Value, Missing.Value);
                nameSpace = null;
            }

            Outlook.Explorer explorer = StateMachine.outlook.Explorers.Add(
                        StateMachine.outlook.Session.GetDefaultFolder(
                        Outlook.OlDefaultFolders.olFolderInbox)
                        as Outlook.Folder,
                        Outlook.OlFolderDisplayMode.olFolderDisplayNormal);

            explorer.Display();
            explorer.WindowState = Outlook.OlWindowState.olMinimized;

            StateMachine.inbox = StateMachine.outlook.ActiveExplorer().Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
            Form1.UpdateActivityLog("Outlook initialization completed");
            StateMachine.test = this;

            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            return true;
        }


    }
}
