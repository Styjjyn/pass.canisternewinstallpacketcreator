﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_CreateExe : ActionState
    {
        private FileInfo issFile;

        public ActionState_CreateExe(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Creating EXE";

        public override string StateDescription => "Running Inno Setup Script to compile a customer-friendly EXE";

        public override bool Enter()
        {
            DirectoryInfo dir = new DirectoryInfo(Constants.PATH_APP_ISS_FOLDER);
            issFile = dir.GetFiles("*.iss").FirstOrDefault();

            if (issFile == null)
            {
                ErrorHandler.Add(new Exception($"No .ISS file was found at: {dir.FullName}. Try re-running the installer."));
                return false;
            }

           

            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            ProcessStartInfo pStartInfo = new ProcessStartInfo();
            pStartInfo.FileName = "cmd.exe";
            pStartInfo.Arguments = "/C cd /d \"C:\\Program Files (x86)\\Inno Setup 5\" &issc.exe \"C:\\Parata Systems\\Pass Canister New Install Packet Creator\\ISS Scripts\\ExeCreatorScript.iss\" ";
            Process p;
            using (p = Process.Start(pStartInfo))
            {


                p.Close();
            }

            return true;
        }
    }
}
