﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_CreateNotAddedfile : ActionState
    {
        public ActionState_CreateNotAddedfile(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Create 'NDCs not added' file (if needed)";

        public override string StateDescription => throw new NotImplementedException();

        

        public override bool Enter()
        {
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            Form1.UpdateActivityLog("Determining need for 'NDCs not included file...'");
            CreateNdcsNotAddedFile();
            Form1.UpdateActivityLog("... finished determining need for 'NDCs not included file'!");
            return true;
        }

        private void CreateNdcsNotAddedFile()
        {
            if (File.Exists(Variables.NDC10FilePath) == true)
            {
                File.Delete(Variables.NDC10FilePath);
            }


            if (Variables.NdcsNotAdded.Count > 0)
            {
                string[] linesToWrite = new string[Variables.NdcsNotAdded.Count];

                for (int i = 0; i < linesToWrite.Length; i++)
                {
                    List<string> list = Variables.NdcsNotAdded[i];
                    for (int j = 0; j < list.Count; j++)
                    {
                        linesToWrite[i] += list[j] + "\t";
                    }
                }

                File.WriteAllLines(Variables.NDC10FilePath, linesToWrite);
            }
            else
            {
                Form1.UpdateActivityLog("File not needed!");
            }

        }
    }
}
