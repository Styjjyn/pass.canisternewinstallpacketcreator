﻿using Canister_New_Install_Packet_Creator;
using System;
using System.IO;


namespace Pass_Canister_NewInstallPacketCreator
{
    public class ActionState_CreateAssetFolder : ActionState
    {
        public ActionState_CreateAssetFolder(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Create Asset Folder";

        public override string StateDescription => throw new NotImplementedException();

        

        public override bool Enter()
        {
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            Form1.UpdateActivityLog("Creating Asset folder on shared drive...");
            CreateMachineFolder();
            Form1.UpdateActivityLog("...created Asset folder on shared drive!");

            return true;
        }

        internal static void CreateMachineFolder()
        {
            Directory.CreateDirectory(Constants.PATH_FOLDER_PACMED + Variables.AssetFolderName + @"\Passware\");
            Variables.MachineFolderPath = Constants.PATH_FOLDER_PACMED + Variables.AssetFolderName + "\\";
            Variables.MachinePasswareFolderPath = Variables.MachineFolderPath + "Passware\\";
            Variables.NDC10FilePath = Variables.MachinePasswareFolderPath + Constants.FILE_NAME_NDCS_NOT_INCLUDED;
        }
    }
}
