﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_PerformPddbConversion : ActionState
    {
        public ActionState_PerformPddbConversion(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Sending Cdd Export to Pddb conversion";

        public override string StateDescription => throw new NotImplementedException();

        

        public override bool Enter()
        {
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        bool readyForNextStep = false;
        public override bool Run()
        {
            Form1.SetCurrentToMarqee();
            Form1.UpdateActivityLog("Requesting Pddb Cdd conversion...");
            Form1.OnPageLoadDo("Canister Drug Data", Stage1);
            Form1.OpenWebBrowserTo("http://pddb/CanisterDrugDB/CDD.aspx");

            while (readyForNextStep == false)
            {

            }

            Form1.SetClipboard(Variables.CustomerXLSFilePath);
            Thread.Sleep(125);
            Form1.ClipboardPaste();
            Thread.Sleep(125);
            Form1._SendKeys("{ENTER}");
            Thread.Sleep(125);
            readyForNextStep = false;
            while (readyForNextStep == false)
            {

            }
            Form1.CloseWebBrowser();
            Form1.UpdateActivityLog("...finished requesting Pddb Cdd conversion!");

            return true;
        }

        private void Stage1(object s, WebBrowserDocumentCompletedEventArgs a)
        {
            HtmlElement fileUploadButton = Form1.GetBrowserInputById("MasterBody_FileUpload1");
            if (fileUploadButton == null)
                return;

            readyForNextStep = true;
            Form1.DocumentCompleted_Rem(Stage1);
            Form1.OnPageLoadDo("", Stage2);
            fileUploadButton.InvokeMember("click");
        }

        private void Stage2(object s, WebBrowserDocumentCompletedEventArgs a)
        {
            
               HtmlElement dropDown = Form1.GetBrowserSelectById("MasterBody_PassUnitDropDownList");
            if (dropDown == null)
                return;

            HtmlElement gobutton = Form1.GetBrowserInputById("MasterBody_GoButton");
            if (gobutton == null)
                return;

           
            Form1.DocumentCompleted_Rem(Stage2);

            switch (Variables.AssetType)
            {
                case "208":
                    dropDown.Children[0].SetAttribute("selected", "selected");
                    break;
                case "240":
                    dropDown.Children[1].SetAttribute("selected", "selected");
                    break;
                case "500":
                    dropDown.Children[2].SetAttribute("selected", "selected");
                    break;
            }

            gobutton.InvokeMember("click");
            Thread.Sleep(2000);
            readyForNextStep = true;
        }

 
    }
}
