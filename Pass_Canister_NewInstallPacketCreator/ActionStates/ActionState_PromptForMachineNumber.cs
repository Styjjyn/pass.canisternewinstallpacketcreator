﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_PromptForMachineNumber : ActionState
    {
        

        public ActionState_PromptForMachineNumber(Procedure fsm) : base(  fsm)
        {

        }

        public override string StateName => "Prompt for Machine Number";

        public override string StateDescription => "Prompt the user to input the Machine Number of the asset the packet is being created for.";

        public override bool Enter()
        {
            return true;

        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            string ret = Form1.ShowTextInputBox("Enter Machine number","Please enter the Machine number in the text field below. Press 'DONE' when done.");

            Variables.MachineNumber = ret;

            return true;
        }
    }
}
