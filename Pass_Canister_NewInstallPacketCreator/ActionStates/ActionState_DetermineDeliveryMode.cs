﻿using Canister_New_Install_Packet_Creator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_DetermineDeliveryMode : ActionState
    {

        const int OPEN_EXISTING = 3;
        const uint GENERIC_READ = 0x80000000;
        const uint GENERIC_WRITE = 0x40000000;
        const uint IOCTL_STORAGE_EJECT_MEDIA = 0x2D4808;

        private DeliveryMode chosenMode;

        public ActionState_DetermineDeliveryMode(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Determine Delivery Mode";

        public override string StateDescription => "Waiting on the user to select a delivery mode";

        public override bool Enter()
        {
            Form1.SetCurrentToMarqee();
            
            //Allow different delivery modes
            //Form1.UpdateActivityLog("Waiting for user input...");
            //DeliveryMode tempMode = (DeliveryMode) Form1.ShowOptionPrompt("Choose...", "How would you like to deliver this to the customer?", new string[] { ((DeliveryMode)0).ToString(), ((DeliveryMode)1).ToString(), ((DeliveryMode)2).ToString() });
            //if (Form1.ShowYesNoPrompt("Confirm", "Really deliver to customer via " + tempMode.ToString() + "?") != true)
            //{
            //    Form1.UpdateActivityLog("...User declined proceed. Repeating last...");
            //    return Enter();
            //}

            //else
            //{
            //    Form1.UpdateActivityLog("...user input received!");
            //    chosenMode = tempMode;
            //    return true;
            //}

            chosenMode = DeliveryMode.USB;
            return true;
        }

        public override bool Exit()
        {
            return true;
        }

        public override bool Run()
        {
            switch (chosenMode)
            {
                case DeliveryMode.CD:
                    return CheckForCd();
                case DeliveryMode.DIGITAL:
                    
                case DeliveryMode.USB:
                    return CheckForUsb();
                default:
                    return false;
            }
        }

        private bool CheckForCd()
        {
            DriveInfo[] drives = DriveInfo.GetDrives().Where((d) => d.DriveType == DriveType.CDRom).ToArray();

            if (drives.Length == 0)
            {
                Form1.ShowMessagePrompt("No CD Drives found!", "Please connect a CD drive to this laptop or PC.\nIf a CD drive IS connected, \nensure that a CD is present within the drive.\nPress DONE after this is accomplished...");
                return CheckForCd();
            }

            string[] options = new string[drives.Length];
            int i = 0;
            foreach (DriveInfo drive in drives)
            {
                options[i] = $"{drive.DriveType} - {drive.Name}";
            }



            int selected = Form1.ShowOptionPrompt("choose...","Please choose the correct CD to store the EXE on.", options);

            DriveInfo selectedDrive = drives[selected];
            
            Form1.ShowMessagePrompt("Selected Drive Info", $"The seklected drive was index {selectedDrive.DriveType}");
            if (selectedDrive.IsReady == false)
            {
                Form1.ShowYesNoPrompt("No CD inserted in drive!","Please insert a blank CD into the drive.\nPress YES when done.");
            }
            Form1.ShowMessagePrompt("Drive Info", $"The seklected drive has {selectedDrive.DriveFormat}");

            return true;
        }

        private bool CheckForUsb()
        {
            Form1.UpdateActivityLog("Checking for removable drives...");
            DriveInfo[] drives = DriveInfo.GetDrives().Where((d) => d.DriveType == DriveType.Removable).ToArray();
            bool ret = false;
            if (drives.Length == 0)
            {
                Form1.UpdateActivityLog("...no removable drives found...");
                Form1.UpdateActivityLog("Prompting user...");
                Form1.ShowMessagePrompt("No Removable Drives found!", "Please connect a Removable drive to this laptop or PC.\nPress DONE after this is accomplished...");
                Form1.UpdateActivityLog("...User continued from prompt. Looping...");
                return CheckForUsb();
            }

            string[] options = new string[drives.Length];
            int i = 0;
            foreach (DriveInfo drive in drives)
            {
                options[i] = $"{drive.DriveType} - {drive.Name}";
            }
            Form1.UpdateActivityLog("Prompting user for drive selection...");
            int selected = Form1.ShowOptionPrompt("choose...", "Please choose the correct Removable Drive to store the EXE on.", options);

            DriveInfo selectedDrive = drives[selected];
            
            ProcessStartInfo pstart = new ProcessStartInfo();
            OperatingSystem os = Environment.OSVersion;
            pstart.FileName = "CMD";
            string cmdWin7 = $"/C format {selectedDrive.Name.Replace("\\", "")} /FS:NTFS /V:CANISTR_MGR_PLUS /Q";
            pstart.UseShellExecute = true;
            pstart.CreateNoWindow = false;
            pstart.WindowStyle = ProcessWindowStyle.Normal;

            Form1.UpdateActivityLog("formatting USB...");
            Process p = Process.Start(pstart);
            using (p)
            {
                p.WaitForExit();

                if (p.ExitCode == 0)
                    ret = true;
                else
                {
                    ret = false;
                    ErrorHandler.Add(new Exception("The format CMD exited with: " + p.ExitCode));
                }

                p.Close();
            }
            Form1.UpdateActivityLog("...finished formatting USB");

            if (ret)
            {
                FileInfo exe = new FileInfo(Constants.FILE_APP_EXE_OUT);
                exe.CopyTo(selectedDrive.Name + Constants.FILE_NAME_APP_EXE_OUT,true);
                EjectDrive(selectedDrive.Name.Replace(":","").Replace("\\","").ToCharArray()[0]);
                
            }

            return ret;
        }

        private bool PerformNasuniStuff()
        {
            return true;
        }

        private enum DeliveryMode
        {
            CD,
            USB,
            DIGITAL
        }

        [DllImport("kernel32")]
        private static extern int CloseHandle(IntPtr handle);

        [DllImport("kernel32")]
        private static extern int DeviceIoControl
            (IntPtr deviceHandle, uint ioControlCode,
              IntPtr inBuffer, int inBufferSize,
              IntPtr outBuffer, int outBufferSize,
              ref int bytesReturned, IntPtr overlapped);

        [DllImport("kernel32")]
        private static extern IntPtr CreateFile
            (string filename, uint desiredAccess,
              uint shareMode, IntPtr securityAttributes,
              int creationDisposition, int flagsAndAttributes,
              IntPtr templateFile);

        static void EjectDrive(char driveLetter)
        {
            string path = "\\\\.\\" + driveLetter + ":";

            IntPtr handle = CreateFile(path, GENERIC_READ | GENERIC_WRITE, 0,
                IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);

            if ((long)handle == -1)
            {
                MessageBox.Show("Unable to open drive " + driveLetter);
                return;
            }

            int dummy = 0;

            DeviceIoControl(handle, IOCTL_STORAGE_EJECT_MEDIA, IntPtr.Zero, 0,
                IntPtr.Zero, 0, ref dummy, IntPtr.Zero);

            CloseHandle(handle);

            MessageBox.Show("OK to remove drive.");
        }
    }
}
