﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Pass_Canister_NewInstallPacketCreator.ActionStates
{
    public class ActionState_WaitForCddEmail : ActionState
    {
        public ActionState_WaitForCddEmail(Procedure fsm) : base(fsm)
        {
        }

        public override string StateName => "Waiting for Cdd Conversion from Pddb";

        public override string StateDescription => throw new NotImplementedException();
     

        public override bool Enter()
        { return true; }

        public override bool Exit()
        { return true; }

        public override bool Run()
        {
            Form1.SetCurrentToMarqee();
            Form1.UpdateActivityLog("Waiting for Cdd mail...");
            StateMachine.inbox.Items.ItemAdd += Items_ItemAdd;
            while (StateMachine.CddMail == null)
            {
                Form1.UpdateActivityLog("...");
                Thread.Sleep(1000);
                
            }
            Form1.UpdateActivityLog("Cdd Mail received!");

            return true;
        }

        private void Items_ItemAdd(object Item)
        {
            Form1.UpdateActivityLog("Mail received...");
            Outlook.MailItem mail = Item as Outlook.MailItem;

            if (mail == null)
                Form1.UpdateActivityLog("...but was null");
            else if (mail.Sender == null)
                Form1.UpdateActivityLog("...but Sender was null");
            else if (mail.Sender.Address == null)
                Form1.UpdateActivityLog("...but Sender Address was null");
            else if (mail.Sender.Address != "DoNotReply@parata.com")
                Form1.UpdateActivityLog($"...but Sender Address was '{mail.Sender.Address}'");
            else if (mail.Subject == null)
                Form1.UpdateActivityLog("...but subject was null");
            else if (mail.Subject != "CDD Files")
                Form1.UpdateActivityLog($"...but Subject was'{mail.Subject}'");

            if (mail != null && mail.Sender != null && mail.Sender.Address != null && mail.Sender.Address == "DoNotReply@parata.com" && mail.Subject == "CDD Files")
            {

                StateMachine.inbox.Items.ItemAdd -= Items_ItemAdd;
                StateMachine.CddMail = mail;
                Form1.UpdateActivityLog("Mail polling stopped.");
            }

        }
    }
}
