﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator
{
    public static class ErrorHandler
    {
        private static List<ErrorInfo> errors = new List<ErrorInfo>();

        public static void Add(Exception e)
        {
            errors.Add(new ErrorInfo(e));
        }

        public static void Add(ErrorInfo error)
        {
            
            errors.Add(error);

        }

        public static void Reset()
        {
            errors = new List<ErrorInfo>();
        }

        public static void LogErrors()
        {
            Form1.UpdateActivityLog("XXXXXXXXXXXXXXXXXXXX");
            Form1.UpdateActivityLog("ERRORS ENCOUNTERED...");
            foreach (ErrorInfo error in errors)
            {
                Form1.UpdateActivityLog(error.ToString());
            }
            Form1.UpdateActivityLog("XXXXXXXXXXXXXXXXXXXX");
        }
    }

    public enum ErrorCategory
    {
        USER,
        SYSTEM,
        UNHANDLED
    }

    public struct ErrorInfo
    {
        public ErrorCategory category;
        public string timeStamp;
        public string stepName, stepDescription, lastActivity;
        public Exception exceptionThrown;

        public ErrorInfo(Exception e)
        {
            category = ErrorCategory.UNHANDLED;
            timeStamp = DateTime.Now.ToString("MM/dd/yyyy - hh:mm:ss.ffff");
            stepName = Procedure.CurrentState.StateName;
            stepDescription = Procedure.CurrentState.StateDescription;
            exceptionThrown = e;
            lastActivity = Form1.LastActivity;
        }

        public new string ToString()
        {
            string ret = "ERROR\n";
            ret += "Category: " + category.ToString() + "\n";
            ret += "TimeStamp: " + timeStamp + "\n";
            ret += $"Step Occured At: {stepName}\n";
            ret += $"Step Description: {stepDescription}\n";
            ret += $"Last Activity Recorded: {lastActivity}\n";
            ret += $"Exception: {exceptionThrown.Message}\n";
            ret += $"Exception Trace: {exceptionThrown.StackTrace}\n";
            ret += "ERROR END";

            return ret;
        }
    }
}
