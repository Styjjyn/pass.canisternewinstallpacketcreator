﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass_Canister_NewInstallPacketCreator
{
    /// <summary>
    /// A runnable state that is enterable by the Finite State Machine
    /// </summary>
    public abstract class ActionState
    {
        
        /// <summary>
        /// A container for storing actions within actions in order to keep code clean
        /// </summary>
        protected ActionState[] subActions;

        /// <summary>
        /// The Name of Action running.
        /// </summary>
        public abstract string StateName { get; } 

        /// <summary>
        /// A description intended for debugging purposes only.
        /// </summary>
        public abstract string StateDescription { get;}

        /// <summary>
        /// The state machine running this state.
        /// </summary>
        private Procedure stateMachine;
        protected Procedure StateMachine { get => stateMachine; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="fsm"></param>
        public ActionState(Procedure fsm)
        {
            stateMachine = fsm;
        }

        /// <summary>
        /// Run the initialization tasks for this Action
        /// </summary>
        /// <returns></returns>
        public abstract bool Enter();

        /// <summary>
        /// Run the core tasks for this Action
        /// </summary>
        /// <returns></returns>
        public abstract bool Run();

        /// <summary>
        /// Run the cleanup tasks for this action
        /// </summary>
        /// <returns></returns>
        public abstract bool Exit();

        
    }
}
