﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canister_New_Install_Packet_Creator
{
    internal static class Constants
    {
        
        internal static readonly string PATH_APP_FOLDER = @"C:\Parata Systems\Pass Canister Packet Creator\";
        internal static readonly string PATH_APP_LOG_FOLDER = PATH_APP_FOLDER + @"Logs\";
        internal static readonly string PATH_APP_EXE_INPUT_FOLDER = PATH_APP_FOLDER + @"EXE Input\";
        internal static readonly string PATH_APP_EXE_OUTPUT_FOLDER = PATH_APP_FOLDER + @"EXE Output\";
        internal static readonly string FILE_NAME_APP_EXE_OUT = "ParataPassPacketInstaller.exe";
        internal static readonly string FILE_APP_EXE_OUT = PATH_APP_EXE_OUTPUT_FOLDER + FILE_NAME_APP_EXE_OUT;
        internal static readonly string PATH_APP_ISS_FOLDER = PATH_APP_FOLDER + "ISS Scripts\\";
        internal static readonly string PATH_INNO_SETUP = @"C:\Program Files (x86)\Inno Setup 5\";
        internal static readonly string FILE_NAME_NDCS_NOT_INCLUDED = "NDCs not included in CDD or preload files.txt";
        internal static readonly string FILE_NAME_SELECTED_DRUG_LIST = "SelectedDrugList.txt";
        internal static readonly string PATH_FOLDER_PACMED = @"\\fserver\Shared\Manufacturing\Production\!Public\Pacmed\";
        internal static readonly string PATH_FOLDER_PRELOAD_PHARMACY = $@"{PATH_FOLDER_PACMED}!PRELOAD AND CDD TEMPLATES\Canister manager plus PHARMACY\";
        internal static readonly string PATH_FOLDER_PRELOAD_DOCUMENTS = $@"{PATH_FOLDER_PACMED}!PRELOAD AND CDD TEMPLATES\Canister Manager Plus Documents\";
        internal static readonly string PATH_FOLDER_PRELOAD_PHARMACY_FILES = $@"{PATH_FOLDER_PRELOAD_PHARMACY}Files\";
        internal static readonly string[] headerName = { "Machine",
                                            "No",
                                            "Mnemonic",
                                            "NDC",
                                            "Generic_Name",
                                            "Commercial_Name",
                                            "Label_Name",
                                            "Strength",
                                            "Type of Tablet",
                                            "Drug_Color",
                                            "Manufacturer",
                                            "Form"};
        internal static readonly string PATH_APP_CONFIG_FOLDER = PATH_APP_FOLDER + "Config\\";


    }
}
