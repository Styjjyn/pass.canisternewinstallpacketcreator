﻿using System;
using Pass_Canister_NewInstallPacketCreator.ActionStates;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Pass_Canister_NewInstallPacketCreator
{

    public class Procedure
    {
        private static Procedure link;

        private ActionState[] states;
        public Outlook.Application outlook;
        public Outlook.MAPIFolder inbox;
        public Outlook.MailItem CddMail;
        public ActionState_OpenOutlookSilently test;
        private ActionState activeAction;
        public static ActionState CurrentState => link.activeAction;
        public int currentActionState;

        public void Start()
        {
            link = this;
            Reset();
            InitializeStates();
            Form1.SetOverallTaskCount(states.Length);
            Next();
        }

        public void Reset()
        {
            
            currentActionState = -1;
            Variables.Reset();
            
        }

        public void InitializeStates()
        {
            states = new ActionState[]
                {
                    new ActionState_ValidatePrereqs(this),
                    new ActionState_PromptForMachineNumber(this),
                    new ActionState_OpenOutlookSilently(this),
                    new ActionState_VerifyMachineNumber(this),
                    new ActionState_CreateAssetFolder(this),
                    new ActionState_CreateNotAddedfile(this),
                    new ActionState_PerformExcelTasks(this),
                    new ActionState_PerformPddbConversion(this),
                    new ActionState_WaitForCddEmail(this),
                    new ActionState_CopyConvertedFiles(this),
                    new ActionState_PrepareExeCreation(this),
                    new ActionState_CreateExe(this),
                    new ActionState_DetermineDeliveryMode(this),
                    new ActionState_Completed(this)
                };
        }

        public void Next()
        {
            currentActionState++;

            if (currentActionState >= states.Length)
                Complete();
            else
            {
                

                try
                {
                    Form1.UpdateState(states[currentActionState].StateName);
                    Form1.ResetCurrentProgress();
                    if (EnterState(currentActionState) == false) return;
                    if (RunState(currentActionState) == false) return;
                    if (ExitState(currentActionState) == false) return;
                    Form1.StepOverallProgress();
                    Next();
                }
                catch (Exception e)
                {
                    ErrorHandler.Add(new ErrorInfo()
                    {
                        category = ErrorCategory.UNHANDLED,
                        exceptionThrown = e,
                        stepName = activeAction.StateName,
                        stepDescription = activeAction.StateDescription,
                        timeStamp = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.ffff")
                    });
                    ErrorOut();
                }
                

                
            }

            
        }

        private bool ErrorOut()
        {

            Form1.UpdateState("FAILURE: see log file");

            if (Form1.ShowYesNoPrompt("FAILURE", "Save errors to a text file?") == true)
                Form1.RemindMe();

            ErrorHandler.LogErrors();

            return false;
        }

        private void Complete()
        {
            if (Form1.ShowYesNoPrompt("Another?", "Would you like to create another install package?"))
            {
                Start();
            }
            else
            {
                Form1.Exit();
            }
        }

        public bool EnterState(int toEnter)
        {
            activeAction = states[toEnter];

            Form1.UpdateActivityLog("ENTERING: " + activeAction.StateName);
            if (activeAction.Enter() == false) return ErrorOut();
            Form1.UpdateActivityLog("ENTERING COMPLETED: " + activeAction.StateName);
            return true;
        }

        public bool RunState(int toRun)
        {
            if (activeAction == null)
                throw new Exception("NO ACTION STATE ASSIGNED");

            Form1.UpdateActivityLog("RUNNING: " + activeAction.StateName);
            if (activeAction.Run() == false) return ErrorOut();
            Form1.UpdateActivityLog("RUNNING COMPLETED: " + activeAction.StateName);
            return true;
        }

        public bool ExitState(int toExit)
        {
            if (activeAction == null)
                throw new Exception("NO ACTION STATE ASSIGNED");

            Form1.UpdateActivityLog("EXITING: " + activeAction.StateName);
            if (activeAction.Exit() == false) return ErrorOut();
            Form1.UpdateActivityLog("EXITED: " + activeAction.StateName);
            return true;
        }

    }

    public enum enum_ActionState
    {
        RESET, //Clear all variables, close access, close outlook
        OPEN_OUTLOOK, //Silently open outlook, begin polling for PDDB email
        VALIDATE_MACH_NUM,//Checks to see that mach num exists in db. Create asset folder in PACMED if so. FAIL if no.
        EXTRACT_ASSET_DATA,//Store asset number, customer name, machine type in THIS
        RUN_ACCESS_QUERY, //Run querry in Access db, output to excel in asset folder
        SCRUB_EXCEL, //ensure any duplicates are removed and placed in 'NDCS not included.txt' in asset folder
        BROWSER_TASKS,//Open internal browser to PDDB, automate
        AWAIT_CONVERSION,//Wait for the PDDB email to be received
        STORE_CONVERSION,//Store the attachments from teh email in the asset folder/PASSWARE
        PREPARE_COMPRESSION,//Store the 'selected Drug List' and 'NDCs not included' in EXE creation folder
        CREATE_EXE,//Run the ISS script to create the EXE in the asset folder
        DELIVERY_SETUP,//Prompt the user to define the delivery method: CD, USB, Digital
        DELIVERY, //enter substate based on setup
        COMPLETION, //Summarize run stats (customer, mch num, mach type, time completion)
        PROMPT_MACHINENUM
    }

    public enum DeliveryState_Nasuni
    {
        OPEN_NASUNI_BROWSER, //Open Nasuni Broswer to the asset folder (where the EXE is)
        CREATE_SHARE_LINK, //Automate share link creation for (1 month expiration, password is 'CustomerName1!')
        CREATE_EMAIL
        
    }

    public enum DeliveryState_CD
    {
        LOCATE_DRIVE,
        PROMPT_USER_FOR_CD,
        COPY_FILES,
        BURN_DISC,
        PROMPT_USER_TO_LABEL,
        COMPLETION
    }

    public enum DeliveryState_USB
    {
        PROMPT_USER_FOR_TARGET,//Open a file browser for the user to select the desired drive
        FORMAT_DRIVE,//Format drive to FAT32 and rename to 'Canister Manager Plus'
        COPY_FILES, //Copy the exe to the Thumb drive
        COMPLETION

    }
}
