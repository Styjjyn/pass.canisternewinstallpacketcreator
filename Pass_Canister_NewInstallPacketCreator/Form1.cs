﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Pass_Canister_NewInstallPacketCreator.ActionStates;

namespace Pass_Canister_NewInstallPacketCreator
{
    public partial class Form1 : Form
    {
        private static Form1 link;

        public Form1()
        {
            InitializeComponent();
            link = this;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// On 'GO' pressed:
        /// Clear all data from application
        /// Begin State machine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Procedure proc = new Procedure();
            label_State.Visible = true;
            cb_ShowActLog.Visible = true;
            gb_Progress.Visible = true;
            button1.Visible = false;
            RunThreadedAction(proc.Start);


        }

        internal static int ShowOptionPrompt(string c, string m,string[] options)
        {
            if (link.InvokeRequired)
            {
                return (int) link.Invoke(new Func<int>(() => ShowOptionPrompt(c,m,options)));
            }

            else
            {
                int ret = -1;

                FlowLayoutPanel layout = null;
                Form oPrompt = GetStandardForm(c,m, ref layout);

                int i = 0;
                foreach (string option in options)
                {
                    int retValue = i;
                    RadioButton rb = new RadioButton();
                    rb.Text = options[i];
                    rb.CheckedChanged += (s, a) =>
                    {
                        if (((RadioButton)s).Checked == true)
                            ret = retValue;
                    };

                    layout.Controls.Add(rb);

                    i++;
                }

                Button b_Done = new Button();
                b_Done.Text = "Next";
                b_Done.Click += (s, a) =>
                {
                    oPrompt.Close();
                };
                layout.Controls.Add(b_Done);

                oPrompt.ShowDialog(link);

                return ret;
            }
        }

        public static void UpdateActivityLog(string entry)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => UpdateActivityLog(entry)));
            }

            else
            {
                Label label = new Label();
                label.Text = DateTime.Now.ToString("hh:mm:ss.fff") + " - " + entry;
                label.AutoSize = true;
                link.logContents.Controls.Add(label);
            }
        }

        public static void UpdateState(string stateName)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(()=> UpdateState(stateName)));
            }

            else
            {
                link.label_State.Text = "Current task: " + stateName;
            }

        }

        public static string LastActivity
        {
            get
            {
                if (link.InvokeRequired)
                {
                    return (string)link.Invoke(new Func<string>(() => LastActivity));
                }
                else
                {
                    return link.logContents.Controls[link.logContents.Controls.Count - 1].Text.Substring(12);
                }
            }
        }

        public static void RemindMe()
        {
            if (link.InvokeRequired)
                link.Invoke(new Action((() => RemindMe())));
            else
            {
                MessageBox.Show(link, "REMINDER", "FINISH THIS", MessageBoxButtons.OK);
            }
        }

        public delegate string DEL_TextInput(string cap, string msg);
        public static string ShowTextInputBox(string caption, string message)
        {
            if (link.InvokeRequired)
            {
                return link.Invoke(new Func<string>(() => ShowTextInputBox(caption, message))) as string;
            }
            else
            {
                string ret = "FAIL";

                FlowLayoutPanel layout = new FlowLayoutPanel();
                Form mBox = GetStandardForm(caption, message, ref layout);

                TextBox textEntry = new TextBox();
                layout.Controls.Add(textEntry);

                Button doneButton = new Button();
                doneButton.Text = "DONE";
                doneButton.Click += (s, a) =>
                    {
                        ret = textEntry.Text;
                        mBox.DialogResult = DialogResult.OK;
                    };
                layout.Controls.Add(doneButton);

                if (mBox.ShowDialog(link) != DialogResult.OK)
                    throw new Exception("FAILED");


                return ret;
            }

        }

        internal static void ShowMessagePrompt(string caption, string message)
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { ShowMessagePrompt(caption, message); }));
            else
            {
                FlowLayoutPanel layout = null;
                Form messagePrompt = GetStandardForm(caption, message, ref layout);

                Button b_Ok = new Button();
                b_Ok.Text = "OK!";
                b_Ok.Anchor = AnchorStyles.None;
                b_Ok.Click += (s, a) =>
                {
                    messagePrompt.Close();
                };
                layout.Controls.Add(b_Ok);

                messagePrompt.ShowDialog(link);
            }

            
        }

        internal static void OpenWebBrowserTo(string path)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => OpenWebBrowserTo(path)));
            }
            else
            {
                link.webBrowser1.Navigate(path);
            }
        }

        public static void OnPageLoadDo(string pagetitle, WebBrowserDocumentCompletedEventHandler a)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => OnPageLoadDo(pagetitle, a)));
            }
            else
            {
                link.webBrowser1.DocumentCompleted += a;
            }
        }

        public static HtmlElement WebBrowserHasInputByName(string name)
        {
            if (link.InvokeRequired)
            {
                return (HtmlElement)link.Invoke(new Func<HtmlElement>(() => WebBrowserHasInputByName(name)));
            }
            else
            {
                HtmlElement ret = null;
                HtmlElementCollection col = link.webBrowser1.Document.GetElementsByTagName("INPUT");

                for (int i = 0; i < col.Count; i++)
                {
                    if (col[i].Name == name)
                        return col[i];
                }


                return ret;
            }
        }

        public static HtmlElement GetBrowserSelectById(string id)
        {
            if (link.InvokeRequired)
            {
                return (HtmlElement)link.Invoke(new Func<HtmlElement>(() => GetBrowserSelectById(id)));
            }
            else
            {
                HtmlElement ret = null;
                HtmlElementCollection col = link.webBrowser1.Document.GetElementsByTagName("SELECT");

                for (int i = 0; i < col.Count; i++)
                {
                    if (col[i].Id == id)
                        return col[i];
                }


                return ret;
            }
        }

        public static void CloseWebBrowser()
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => CloseWebBrowser()));
            }
            else
            {
                link.webBrowser1.Hide();
                link.webBrowser1 = null;
            }
        }

        public static HtmlElement GetBrowserInputById(string id)
        {
            if (link.InvokeRequired)
            {
                return (HtmlElement)link.Invoke(new Func<HtmlElement>(() => GetBrowserInputById(id)));
            }
            else
            {
                HtmlElement ret = null;
                HtmlElementCollection col = link.webBrowser1.Document.GetElementsByTagName("INPUT");

                for (int i = 0; i < col.Count; i++)
                {
                    if (col[i].Id == id)
                        return col[i];
                }


                return ret;
            }
        }

        public static void DocumentCompleted_Rem(WebBrowserDocumentCompletedEventHandler a)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => DocumentCompleted_Rem(a)));
            }
            else
            {
                link.webBrowser1.DocumentCompleted -= a;
            }
        }

        public static void RunThreadedAction(Action toRun)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new MethodInvoker(() => { RunThreadedAction(toRun); }));
            }
            else
            {
                link.bgw_FSM = new BackgroundWorker();
                link.bgw_FSM.DoWork += (s, a) => { toRun(); };
                link.bgw_FSM.RunWorkerCompleted += (s, a) => { UpdateActivityLog("FSM thread exited"); };
                link.bgw_FSM.RunWorkerAsync();
            }
        }

        public static void RunSubThread(Action toRun, RunWorkerCompletedEventHandler onComplete)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new MethodInvoker(() => { RunSubThread(toRun, onComplete); }));
            }
            else
            {
                link.bgw_State = new BackgroundWorker();
                link.bgw_State.DoWork += (s, a) => { toRun(); };
                link.bgw_State.RunWorkerCompleted += onComplete;
                link.bgw_State.RunWorkerAsync();
            }
        }

        public delegate bool DEL_YesNo(string cap, string msg);
        public static bool ShowYesNoPrompt(string caption, string message)
        {
            if (link.InvokeRequired)
            {
                return (bool)link.Invoke(new Func<bool>(() => ShowYesNoPrompt(caption, message)));
            }
            else
            {
                bool ret = false;

                ret = MessageBox.Show(link, message, caption, MessageBoxButtons.YesNo) == DialogResult.Yes;
                return ret;
            }
        }

        public static Form GetStandardForm(string caption, string message, ref FlowLayoutPanel layout)
        {
            Form mBox = new Form();
            mBox.Text = caption;
            mBox.AutoSize = true;
            mBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            mBox.ControlBox = false;

            layout = new FlowLayoutPanel();
            layout.FlowDirection = FlowDirection.TopDown;
            layout.AutoSize = true;
            layout.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            mBox.Controls.Add(layout);

            Label messageLabel = new Label();
            messageLabel.Text = message;
            messageLabel.Anchor = AnchorStyles.None;

            messageLabel.AutoSize = true;
            layout.Controls.Add(messageLabel);

            return mBox;
        }



        public static void Exit()
        {

            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => Exit()));
            }
            else {
                link.Close();
            }


        }

        public static void SetClipboard(string text)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => SetClipboard(text)));
            }
            else
            {
                Clipboard.SetText(text);
            }
        }

        public static void _SendKeys(string text)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => _SendKeys(text)));
            }
            else
            {
                SendKeys.Send(text);
            }
        }

        public static void ClipboardPaste()
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new Action(() => ClipboardPaste()));
            }
            else
            {
                SendKeys.Send("^V");
            }
        }

        public static void SetCurrentToMarqee()
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { SetCurrentToMarqee(); }));
            else
            {

                link.pb_Current.Style = ProgressBarStyle.Marquee;
            }
        }

        public static void ResetCurrentProgress()
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { ResetCurrentProgress(); }));
            else
            {
                link.pb_Current.Style = ProgressBarStyle.Blocks;
                link.pb_Current.Value = 0;
            }
        }

        public static void StepCurrentProgress()
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { StepCurrentProgress(); }));
            else
            {
                link.pb_Current.Style = ProgressBarStyle.Blocks;
                link.pb_Current.Value++;
            }
        }

        public static void StepOverallProgress()
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { StepOverallProgress(); }));
            else
            {

                link.pb_Overall.Value ++;
            }
        }

        public static void SetOverallTaskCount(int count)
        {
            if (link.InvokeRequired)
                link.Invoke(new Action(() => { SetOverallTaskCount(count); }));
            else
            {
                link.pb_Overall.Maximum = count;
                link.pb_Overall.Value = 0;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void cb_ShowActLog_CheckedChanged(object sender, EventArgs e)
        {
            gb_ActivityLog.Visible = cb_ShowActLog.Checked;
        }
    }
}
