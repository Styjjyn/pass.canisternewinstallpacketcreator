﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Pass_Canister_NewInstallPacketCreator
{
    public static class Variables
    {

        public static string MachineNumber;
        public static string AssetFolderName;
        internal static string CustomerName;
        internal static string AssetNumber;
        internal static string AssetType = "208";
        internal static List<List<string>> CddExportData = new List<List<string>>();
        internal static List<List<string>> NdcsNotAdded = new List<List<string>>();
        internal static string CustomerXLSFilePath;
        internal static string MachineFolderPath;
        internal static string MachinePasswareFolderPath;
        internal static string NDC10FilePath;

        public static void Reset()
        {
            MachineNumber = null;
            AssetFolderName = null;

        }
    }
}
