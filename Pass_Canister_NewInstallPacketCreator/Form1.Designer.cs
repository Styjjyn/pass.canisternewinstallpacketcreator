﻿namespace Pass_Canister_NewInstallPacketCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_State = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bgw_FSM = new System.ComponentModel.BackgroundWorker();
            this.bgw_State = new System.ComponentModel.BackgroundWorker();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.gb_ActivityLog = new System.Windows.Forms.GroupBox();
            this.logContents = new System.Windows.Forms.FlowLayoutPanel();
            this.cb_ShowActLog = new System.Windows.Forms.CheckBox();
            this.pb_Overall = new System.Windows.Forms.ProgressBar();
            this.pb_Current = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gb_Progress = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gb_ActivityLog.SuspendLayout();
            this.gb_Progress.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_State
            // 
            this.label_State.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_State.AutoSize = true;
            this.label_State.Location = new System.Drawing.Point(4, 12);
            this.label_State.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_State.MinimumSize = new System.Drawing.Size(450, 0);
            this.label_State.Name = "label_State";
            this.label_State.Size = new System.Drawing.Size(450, 20);
            this.label_State.TabIndex = 0;
            this.label_State.Text = "State:";
            this.label_State.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(462, 5);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 35);
            this.button1.TabIndex = 2;
            this.button1.Text = "START";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(454, 250);
            this.webBrowser1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(30, 31);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(381, 345);
            this.webBrowser1.TabIndex = 11;
            this.webBrowser1.Visible = false;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // gb_ActivityLog
            // 
            this.gb_ActivityLog.Controls.Add(this.logContents);
            this.gb_ActivityLog.Location = new System.Drawing.Point(24, 219);
            this.gb_ActivityLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gb_ActivityLog.Name = "gb_ActivityLog";
            this.gb_ActivityLog.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gb_ActivityLog.Size = new System.Drawing.Size(422, 385);
            this.gb_ActivityLog.TabIndex = 12;
            this.gb_ActivityLog.TabStop = false;
            this.gb_ActivityLog.Text = "Activity Log";
            this.gb_ActivityLog.Visible = false;
            // 
            // logContents
            // 
            this.logContents.AutoScroll = true;
            this.logContents.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.logContents.Location = new System.Drawing.Point(10, 31);
            this.logContents.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.logContents.Name = "logContents";
            this.logContents.Size = new System.Drawing.Size(381, 345);
            this.logContents.TabIndex = 1;
            this.logContents.WrapContents = false;
            // 
            // cb_ShowActLog
            // 
            this.cb_ShowActLog.AutoSize = true;
            this.cb_ShowActLog.Location = new System.Drawing.Point(24, 187);
            this.cb_ShowActLog.Name = "cb_ShowActLog";
            this.cb_ShowActLog.Size = new System.Drawing.Size(168, 24);
            this.cb_ShowActLog.TabIndex = 13;
            this.cb_ShowActLog.Text = "Show Activity Log?";
            this.cb_ShowActLog.UseVisualStyleBackColor = true;
            this.cb_ShowActLog.Visible = false;
            this.cb_ShowActLog.CheckedChanged += new System.EventHandler(this.cb_ShowActLog_CheckedChanged);
            // 
            // pb_Overall
            // 
            this.pb_Overall.Location = new System.Drawing.Point(102, 61);
            this.pb_Overall.Name = "pb_Overall";
            this.pb_Overall.Size = new System.Drawing.Size(472, 33);
            this.pb_Overall.TabIndex = 14;
            // 
            // pb_Current
            // 
            this.pb_Current.Location = new System.Drawing.Point(102, 21);
            this.pb_Current.Name = "pb_Current";
            this.pb_Current.Size = new System.Drawing.Size(472, 34);
            this.pb_Current.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 61);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "OVERALL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Current:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 20);
            this.label3.TabIndex = 18;
            // 
            // gb_Progress
            // 
            this.gb_Progress.Controls.Add(this.pb_Overall);
            this.gb_Progress.Controls.Add(this.label1);
            this.gb_Progress.Controls.Add(this.label2);
            this.gb_Progress.Controls.Add(this.pb_Current);
            this.gb_Progress.Location = new System.Drawing.Point(24, 62);
            this.gb_Progress.Name = "gb_Progress";
            this.gb_Progress.Size = new System.Drawing.Size(587, 100);
            this.gb_Progress.TabIndex = 19;
            this.gb_Progress.TabStop = false;
            this.gb_Progress.Text = "PROGRESS";
            this.gb_Progress.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.label_State);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(30, 13);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(578, 45);
            this.flowLayoutPanel1.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(855, 611);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.gb_Progress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cb_ShowActLog);
            this.Controls.Add(this.gb_ActivityLog);
            this.Controls.Add(this.webBrowser1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Pass Canister New Install Packet Creator";
            this.gb_ActivityLog.ResumeLayout(false);
            this.gb_Progress.ResumeLayout(false);
            this.gb_Progress.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_State;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker bgw_FSM;
        private System.ComponentModel.BackgroundWorker bgw_State;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.GroupBox gb_ActivityLog;
        private System.Windows.Forms.FlowLayoutPanel logContents;
        private System.Windows.Forms.CheckBox cb_ShowActLog;
        private System.Windows.Forms.ProgressBar pb_Overall;
        private System.Windows.Forms.ProgressBar pb_Current;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gb_Progress;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}

